##@file.DC_motor.py

'''@DC_motor.py
'''

class MotorDriver:
    ''' This class implements a motor driver for the
    ME405 board. '''
    
    def __init__ (self, EN_pin, IN1_pin, IN2_pin, timer):
        ''' Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
        @param EN_pin A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timer A pyb.Timer object to use for PWM generation on IN1_pin
        and IN2_pin. '''

        self.EN_pin = EN_pin
        self.IN1_pin = IN1_pin
        self.IN2_pin = IN2_pin
        self.timer = timer
        
        print ('Creating a motor driver')
        
        
    def enable (self):
        '''Enables the motor to recive PWM signals and gives the messsage that 
        the motor is now enabled to be driven'''
        print ('Enabling Motor')
        self.EN_pin.high()
        
        
        
    def disable (self):
        '''Disables the motor by sending a PWM signal of 0 to both input pins 
        that were chosen to drive the motor'''
        print ('Disabling Motor')
        self.tmch1.pulse_width_percent(0)
        self.tmch2.pulse_width_percent(0)
        


    def set_duty (self, duty):
        ''' This method sets the duty cycle to be sent
        to the motor to the given level. Positive values
        cause effort in one direction, negative values
        in the opposite direction.
        @param duty A signed integer holding the duty
        cycle of the PWM signal sent to the motor '''
        # both timer channels are sent a 0 pwm signal to turn the DC motor off
        if duty == 0:
            tmch1= self.timer.channel(1, pyb.Timer.PWM, pin=self.IN1_pin)
            tmch1.pulse_width_percent(0)
            
            
            tmch2= self.timer.channel(2, pyb.Timer.PWM, pin=self.IN2_pin)
            tmch2.pulse_width_percent(0)
            

        # timer channel 1: ccw + (1-100)%. Channel 2 is sent a PWM of 0 
        # channel 1 is sent the duty cycle.The same thing is done for the 
        # to spin the motor counter clock wise, however timer channel that 
        # recives the PWM of 0 and the duty cycle are reversed.
        elif duty>1:
            tmch1= self.timer.channel(1, pyb.Timer.PWM, pin=self.IN1_pin)
            tmch2= self.timer.channel(2, pyb.Timer.PWM, pin=self.IN2_pin)
            
            tmch1.pulse_width_percent(0)
            tmch2.pulse_width_percent (duty)
            
            
        # timer channel 2: cw  - (1-100)%
        else:
            tmch1= self.timer.channel(1, pyb.Timer.PWM, pin=self.IN1_pin)
            tmch2= self.timer.channel(2, pyb.Timer.PWM, pin=self.IN2_pin)
            
            tmch1.pulse_width_percent(abs(duty))
            tmch2.pulse_width_percent(0)
        

if __name__ == '__main__':
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    import pyb
    # Create the pin objects used for interfacing with the motor driver
    # PIN for EN can be either A10 for the A-side or C1 for the B-side
    pin_EN = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP) ;
    # PIN for IN1 can be either B4 for the A-side or A0 for the B-side
    pin_IN1 = pyb.Pin (pyb.Pin.cpu.B4);
    # PIN for IN2 can be either B5 for the A-side or A1 for the B-side
    pin_IN2 = pyb.Pin (pyb.Pin.cpu.B5);

    # Create the timer object used for PWM generation
    # If the B-PINS are used the timer channel must be 3
    # If the A-Pins are used the timer channel must be 5
    tim = pyb.Timer(3, freq =20000);
    
    # Create a motor object passing in the pins and timer
    moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)
    
    # Enable the motor driver
    moe.enable()
    
    # Set the duty cycle to 10 percent
    moe.set_duty(0)
