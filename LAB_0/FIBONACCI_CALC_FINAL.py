# -*- coding: utf-8 -*-
"""
Created on Mon Apr 13 12:21:40 2020

@author: impor
"""
'''@FIBONACCI_CALC_V2.py'''


# defining the loop which will be used to calculate the fibonacci number 
# the variable idx is used to in this loop to store the fibonacci being asked
# for.
def fib(idx):
    ''' This method calculates a Fibonacci number corresponding to
       a specified index.@param idx An integer specifying the index of the 
       desiredFibonacci number.'''  
    print('Calculating Fibonacci number at '
           'index n = {:}.'.format(idx))        
       
# if the entered value is less than 2 then the returned value is the value 
# that was entered. If the vlaue entered is greater than 2 then the Fibonacci 
# number is calculated using the basic equation of fn=(fn-1)+(fn-2)
# the "i" is used to count to the fibonacci being asked for by the user the 
# basic outline of this code was modeled after the example given in 
# section 4.6 of the official python  3.8.2 documentaion
    if idx < 2:
        b=idx
    else:
        a, b, i = 0, 1, 0 
        while i < idx:
            a, b = b, a+b
            i = i+1
    return b
    # "user_in" is the variable that stores the entered value
user_in=input('Would you like to enter a number (y)es, (n)o? ')

if __name__ == '__main__':
    # Initiates the loop and keeps the loop running unless loop is broken 
    while (True): 
        # Checking to see what was entered. If anything other than "y" or "n"
        # are entered then the user is told that the entry is not valid 
        # and then asked if they would like to enter 
        # another number if yes then the user is asked to enter a value. If the
        # value entered is not valid user is again asked if they would like to
        # enter another number. if the user enters "n" when asked if they would
        # like to enter another number then the code breaks and stops running
        # and gives the messge" Have a nice day"
            if (user_in =='y' or user_in=='Y'): 
                    idx= input ('Enter a number: ')
                    if (idx.isdigit()):
                        print (fib(int(idx))) 
                        user_in=input('Would you like to enter another number (y)es, (n)o? ')
                    else: 
                        print('Entry not Valid') 
                        user_in=input('Would you like to enter another number (y)es, (n)o? ')
                        
            elif (user_in == 'n' or user_in=='N'):
                print ('Have a nice day')
                break
            else:
                (print ('Entry not valid'))
                user_in=input('Would you like to enter another number (y)es, (n)o? ')

