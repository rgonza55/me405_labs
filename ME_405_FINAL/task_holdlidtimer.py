'''
@file       task_7.py
@brief      Hold Lid Timer task #7
@details    This file is an example dummy task showing how to implement a task
            using a Python class.
            
@page       page_task_7 HoldLidTimer

@brief      Task 1 Documentation

@details

@section    sec_task_2_intro Introduction
            This task prints a timestamp every 1 second.

@section    sec_task_7_fsm Finite State Machine
            This task is implemented using a finite state machine with one
            state.
@subsection sec_task_7_trans State Transition Diagram
@image      html task_7_fsm.svg width=600px

@section    sec_task_7_imp Implementation
            This task is implemented by the HoldLidTimer class.
'''

import utime

class HoldLidTimer:
    '''
    @brief      HoldLidTimer
    @details    This class defines a dummy task to use as an example. The task
                simply prints a timestamp to the terminal every 1 second.
    '''
    
    def __init__(self):
        '''
        @brief      Constructor for task 7
        @details    This constructor is where any initialization code would run
                    to configure the task before it starts operating. This isn't
                    really the same as an "init" state that you would have in a
                    finite state machine. Its more about setting up Python
                    objects that you will need to use in your task.
        '''
        
        print('Created HoldLidTimer Object')
        
        ## @brief   A run counter for task 1
        #  @details Describes the number of times that the task has been
        #           executed by the task loop running in main. This counter can
        #           be used to perform things at a regualr interval as shown in
        #           the run function below.
        #
        self.runs = 0

        self.counter = 0
        
    def run(self):
        '''
        @brief      HoldLidTimer run function
        @details    This method executes one single iteration of task 1. It is
                    up to you how you structure the code within this method, but
                    a finite state machine would be a good implementation if the
                    task is sufficiently complicated.
                    
                    This task only does anything nontrivial if the run counter
                    is an integer multiple of 100. This forces the print
                    statement below to run about once per second because
                    @f[ 100 \times 10000~uS = 1~s @f]
        '''
        global holdlid_flag, openlid_flag  ## add to global in main

        if holdlid_flag == 1:
            self.counter += 1
            if self.counter % 1000 == 0:    ## if reached 10 seconds
                self.counter = 0
                holdlid_flag = 0
                openlid_flag = 1
                print("lid held for 10 seconds")

        else:
            pass
