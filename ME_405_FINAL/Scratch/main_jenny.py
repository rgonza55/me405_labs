# import modules
# Init encoder, motor drivers 1 and 2, kpcontrol, keypad, hall switch

# init timers

#---------------------------------------------------
# state functions
def go_home():
	print("go_home")
# 	state = move_lef
#
#
def idle():
	print("idle")
# 	prev_state = current_state
# 	current_state = idle
#
# 	if candle1 == 1:
# 		current_state = move_candle1
# 	elif candle2 == 2:
# 		current_state = move_candle2
#
def close_cand1():
# 	if candle2 flag is set then:
# 		current_state = candle2
# 	else:
# 		move the motors	to position 1
#
#
def close_cand2():
# 	if candle1 flag is set then:
# 		current_state = candle1
# 	else:
# 		move the motors	to position 1
#-----------------------------------------------------
# Callbacks:
# def timer1_callback()
# 	deinit flag1
# 	set flag1
#
#
#
# def timer2_callback()
# 	deinit timer
# 	set flag2
#
# callbacks from button press:
def set_timer1():
	print("set_timer1")
# 	start countdown1
#
#
def set_timer2():
	print("set_timer2")
# 	start countdown2
#
#

# Global Variables:
# current_state = move_left
# prev_state = move_left
# candle 1
# candle 2
# home
current_state = "go_home"
prev_state = None
# need a try and exception for this in the case a key doesn't exist

keys_dict = {'1': set_timer1,
		'2': set_timer2,
		'go_home' : go_home,
		'idle'    : idle,
		'close_1' : close_cand1,
		'close_2' : close_cand2}

# keys['1']()
# keys['home']()


# create modules:
# keypad = Keypad()
# keypad.start()

while True:
	# try:
	# 	## assuming get_key() doesn't save last key press
	# 	keys_dict[key]()
	#
	# except:
	# 	print("no key press")	## COMMENT THIS OUT LATER

	key = keypad.get_key()
	if (key == '1') or (key == '2'):
		keys_dict[key()]
	elif key == 'D':
		print("quitting program")
		break
	elif current_state == prev_state:
		pass
	elif current_state == "idle":
		idle()
	elif current_state == "go_home":
		go_home()
	elif current_state == "close_cand1":
		close_cand1()
	elif current_state == "close_cand2":
		close_cand2()
	else:
		print("skipped all statements")
		break

## get key at the end?
	# key = keypad.get_key()

#
# 	if yes, dictionary_val = keypress
#
#
#
#
# 	elif candle 1 is pressed:
# 		prev_state = current_state
# 		current_state = move_candle1
#
#
# 	elif candle 2 is pressed:
# 		current_state = move_candle2
#
# 	elif current_state = prev_state
# 	pass
#
# else , dictionary_val = current_state
#
#
