# import testingmachinetimer
import machine

from keypad import Keypad
from motordriver import*
from Hall import*
from encoder import* 

# import modules
# Init encoder, motor drivers 1 and 2, kpcontrol, keypad, hall switch

#initialization for the motor_1 and encoder_1------------------------
pin_en = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)
pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)
pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)

# Create the timer object used for PWM generation
tim = pyb.Timer(3, freq = 20000)

# Create a motor object passing in the pins and timer
moe = MotorDriver(pin_en, pin_IN1, pin_IN2, tim)

# Setup for Motor_1 Encoder
pin_B6 = pyb.Pin(pyb.Pin.cpu.B6)
pin_B7 = pyb.Pin(pyb.Pin.cpu.B7)
setperiod = 200
timer4 = pyb.Timer(4, prescaler=0, period=setperiod)

# 1st encoder driver object:
enc1 = Encoder(pin_B6, pin_B7, timer4, setperiod)
# initalization for the Hall effect sensor------------------------------
pin_A4= pyb.Pin(pyb.Pin.cpu.A4, pyb.Pin.IN)
hall_sensor = HallEffectSensor(pin_A4)

# initialization for motor_2 and encoder_2----------------------------
pin_en = pyb.Pin(pyb.Pin.cpu.C1, pyb.Pin.OUT_PP)
pin_IN1 = pyb.Pin(pyb.Pin.cpu.A0, pyb.Pin.OUT_PP)
pin_IN2 = pyb.Pin(pyb.Pin.cpu.A1, pyb.Pin.OUT_PP)

# Create the timer object used for PWM generation
tim = pyb.Timer(5, freq = 20000)

# Create a motor object passing in the pins and timer
moe = MotorDriver(pin_en, pin_IN1, pin_IN2, tim)

# Setup for Motor 2 Encoder
pin_C6 = pyb.Pin(pyb.Pin.cpu.C6)
pin_C7 = pyb.Pin(pyb.Pin.cpu.C7)
period = 65535
timer8 = pyb.Timer(8, prescaler=0, period=setperiod)

# 2nd encoder driver object
enc2 = Encoder(pin_C6, pin_C7, timer8, setperiod)

# initalization

# init timers
# Countdown Timers:
countdown1 = machine.Timer()
countdown2 = machine.Timer()


#---------------------------------------------------
# state functions
def go_home():
    global current_state, prev_state
    print("go_home")
    while hall_sensor.OnOff() == 1:
        moe.set_duty(100)
        hall_sensor.OnOff()
    moe.set_duty(0)
    prev_state = current_state
    current_state = "idle"


def idle():
    global current_state, prev_state
    print("idle")
    prev_state = current_state
    current_state = "idle"

    if cand1 == 1:
        prev_state = current_state
        current_state = "close_cand1"

    elif cand2 == 1:
        prev_state = current_state
        current_state = "close_cand2"


def close_cand1():
    ## need a flag called closing or something so that prev_state == current_state
    ## while everything is still in progress
    countdown1.deinit()
    print("closing candle 1")
    global cand1, cand2, current_state, prev_state
    cand1 = 1

    if cand2:
        current_state = "close_cand2"
    elif prev_state == "idle":
        move_motors(1)  # move to position 1
        cand1 = 0


def close_cand2():
    countdown2.deinit()
    print("closing candle 2")
    global cand1, cand2, current_state, prev_state
    cand2 = 1

    if cand1:
        current_state = "close_cand1"
    elif prev_state == "idle":
        move_motors(2)  # move to position 2
        cand2 = 0

#-----------------------------------------------------
# Helper functions:

def move_motors(pos_val):
    '''Takes pos_val as either an int 1 or 2 and moves the motors to close candle.'''
    pass



#-----------------------------------------------------
# Callbacks:

# callbacks from button press:
def set_timer1():
    '''Initializes a countdown timer that interrupts to callback close_candl1() when
    it reaches 0 from period value in ms'''
    ## NOTE!!!! period might be affected by other functions running
    print("setting countdown timer 1")
    countdown1.init(mode = countdown1.ONE_SHOT, period = 30000, callback = timer1_callback)
    ## might want to have a flag so the timer doesn't restart if btn1 gets pressed again


def set_timer2():
    '''Initializes a countdown timer that interrupts to callback close_candl2() when
    it reaches 0 from period value in ms'''
    print("setting countdown timer 2")
    countdown1.init(mode = countdown1.ONE_SHOT, period = 30000, callback = timer2_callback)

# callbacks from countdown timers:
def timer1_callback():
    global current_state
    current_state = "close_cand1"


def timer2_callback():
    global current_state
    current_state = "close_cand2"


#------------------------------------------------------
# Global Variables:
cand1 = 0
cand2 = 0
# home
current_state = "go_home"
prev_state = None
# need a try and exception for this in the case a key doesn't exist

keys_dict = {'1': set_timer1,
        '2': set_timer2,
        'go_home' : go_home,
        'idle'    : idle,
        'close_1' : close_cand1,
        'close_2' : close_cand2}

# keys['1']()
# keys['home']()


# create modules:
keypad = Keypad()
# keypad.start()

while True:
    keypad.start()	## not sure if this is problematic
    # try:
    # 	## assuming get_key() doesn't save last key press
    # 	keys_dict[key]()
    #
    # except:
    # 	print("no key press")	## COMMENT THIS OUT LATER

    key = keypad.get_key()
    keypad.stop()
    if (key == '1') or (key == '2'):
        keys_dict[key]()
    elif key == 'D':
        print("quitting program")
        break
    elif current_state == prev_state:
        pass
    elif current_state == "idle":
        idle()
    elif current_state == "go_home":
        go_home()
    elif current_state == "close_cand1":
        close_cand1()
    elif current_state == "close_cand2":
        close_cand2()
    else:
        print("Error: skipped all statements")
        break

## get key at the end?
    # key = keypad.get_key()

#
# 	if yes, dictionary_val = keypress
#
#
#
#
# 	elif candle 1 is pressed:
# 		prev_state = current_state
# 		current_state = move_candle1
#
#
# 	elif candle 2 is pressed:
# 		current_state = move_candle2
#
# 	elif current_state = prev_state
# 	pass
#
# else , dictionary_val = current_state
#
#
