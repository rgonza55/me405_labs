'''
@file       task_timer2.py
@brief      Example task #3
@details    This file is an example dummy task showing how to implement a task
            using a Python class.
            
@page       page_task_3 Task 3

@brief      Task 2 Documentation

@details

@section    sec_task_3_intro Introduction
            This task prints a timestamp every 5 second.

@section    sec_task_3_fsm Finite State Machine
            This task is implemented using a finite state machine with one
            state.
@subsection sec_task_3_trans State Transition Diagram
@image      html task_3_fsm.svg width=600px

@section    sec_task_3_imp Implementation
            This task is implemented by the Task_3 class.
'''

import utime

class Timer2():
    '''
    @brief      Task 2
    @details    This class defines a dummy task to use as an example. The task
                simply prints a timestamp to the terminal every 5 seconds.
    '''
    
    def __init__(self):
        '''
        @brief      Constructor for task 3
        @details    This constructor is where any initialization code would run
                    to configure the task before it starts operating. This isn't
                    really the same as an "init" state that you would have in a
                    finite state machine. Its more about setting up Python
                    objects that you will need to use in your task.
        '''

        print('Created Timer2 Object')
        self.key = key
        ## @brief   A run counter for task 3
        #  @details Describes the number of times that the task has been
        #           executed by the task loop running in main. This counter can
        #           be used to perform things at a regualr interval as shown in
        #           the run function below.
        #
        self.counter = 0

        self.counting_flag = 0

    def run(self):
        '''
        @brief      Task 3 run function
        @details    This method executes one single iteration of task 2. It is
                    up to you how you structure the code within this method, but
                    a finite state machine would be a good implementation if the
                    task is sufficiently complicated.

                    This task only does anything nontrivial if the run counter
                    is an integer multiple of 500. This forces the print
                    statement below to run about once per second because
                    @f[ 500 \times 10000~uS = 5~s @f]
        '''
        global key, move_candle2_flag

        if (key == '2') and self.counting_flag == 0:
            self.counting_flag = 1

        elif self.counting_flag == 1:
            self.counter += 1
            if self.counter % 3000 == 0:    ## if reached 30 seconds
                self.counter = 0
                self.counting_flag = 0
                move_candle2_flag = 1
                print("reached 30 seconds")

