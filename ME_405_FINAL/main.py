'''
@file       main.py
@brief      Main Script File
@details    Main script file that runs continuously. The only thing that
            happens in this file is initializing tasks and then idefinitely
            running the task loop at a fixed interval. This could be considered
            a "round-robin" scheduler, which is a cooperative multi-tasking
            scheme.
            
            Each task should be specified in a seperate file  using a Python 
            class called. The class must contain a function called @c run that
            performs one single iteration of the task.
'''
import utime
from keypad import Keypad
import pyb
from motordriver import MotorDriver
from encoder import Encoder
from Hall import HallEffectSensor
from kpcontrol import KPControl

# Global variables
key = '1'  # string int
keypad = Keypad()
keypad.start()
test_flag = 0
move_candle1_flag = 0
move_candle2_flag = 0
stay_flag = 0
moving1_flag = 0
moving2_flag = 0
closelid_flag = 0
openlid_flag = 0
gohome_flag = 1
movinglid_flag = 0
holdlid_flag = 0
athome_flag = 0

# global vairables
kp = 0.75
setpoint = 0
actuation = KPControl(setpoint, kp)

# initialization of all pins--------------------------------------------------

# setting up the pins for motor1 (moeplat) and encoder 1 (encplat)--------------------------------

pin_enA = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)
pin_IN1A = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)
pin_IN2A = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)

# Create the timer object used for PWM generation
timA = pyb.Timer(3, freq=20000)

moeplat = MotorDriver(pin_enA, pin_IN1A, pin_IN2A, timA)

pin_B6 = pyb.Pin(pyb.Pin.cpu.B6)
pin_B7 = pyb.Pin(pyb.Pin.cpu.B7)
setperiod = 65535
timer4 = pyb.Timer(4, prescaler=0, period=setperiod)

# 1st encoder driver object:
encplat = Encoder(pin_B6, pin_B7, timer4, setperiod)

# -----------------------------------------------------------------------------
# setting up the pins for motor 2 (moelid) and encoder 2 (enclid)-------------------------------
pin_enB = pyb.Pin(pyb.Pin.cpu.C1, pyb.Pin.OUT_PP)
pin_IN1B = pyb.Pin(pyb.Pin.cpu.A0, pyb.Pin.OUT_PP)
pin_IN2B = pyb.Pin(pyb.Pin.cpu.A1, pyb.Pin.OUT_PP)

# Create the timer object used for PWM generation
timB = pyb.Timer(5, freq=20000)

moelid = MotorDriver(pin_enB, pin_IN1B, pin_IN2B, timB)

pin_C6 = pyb.Pin(pyb.Pin.cpu.C6)
pin_C7 = pyb.Pin(pyb.Pin.cpu.C7)
period = 65535
timer8 = pyb.Timer(8, prescaler=0, period=setperiod)

# 2nd encoder driver object
enclid = Encoder(pin_C6, pin_C7, timer8, setperiod)
enclid.zero()
# -----------------------------------------------------------------------------

# setting up the Hall effect sensor
pin_A4 = pyb.Pin(pyb.Pin.cpu.A4, pyb.Pin.IN)

hall_sensor = HallEffectSensor(pin_A4)

testflag = 1
# -----------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# Tasks:

class CheckBtn:
    '''
    @brief      Task 1: CheckBtn
    @details    This class defines a dummy task to use as an example. The task
                simply prints a timestamp to the terminal every 1 second.
    '''

    def __init__(self):
        '''
        @brief      Constructor for task 1
        @details    This constructor is where any initialization code would run
                    to configure the task before it starts operating. This isn't
                    really the same as an "init" state that you would have in a
                    finite state machine. Its more about setting up Python
                    objects that you will need to use in your task.
        '''

        print('Created CheckBtn Object')

    def run(self):
        '''
        @brief      Task: CheckBtn 1 run function
        @details    This method always saves a key value from the keypad.
        '''
        global testflag, athome_flag
        if testflag == 1:
            print("getting key")
            global key, keypad
            keypad.start()
            key = keypad.get_key()
            keypad.stop()
            utime.sleep_ms(10)
            if key:
                print("key is: ", key)
                testflag = 0
            if key == '1':
                athome_flag = 0


class Timer1():
    '''
    @brief      Timer1
    @details    This class defines Timer1 task. In this task, if the most recent
                key press was a '1' and the timer isn't already set, then starts
                the timer. The timer counts for 12 seconds. Then the counting_flag
                clears and the move_candle1_flag gets set for the MoveToCandle task.
    '''

    def __init__(self):
        '''
        @brief      Constructor for Timer1
        @details    This constructor initializes the self.counter and self.counting_flag
                    variables.
        '''

        print('Created Timer1 Object')

        ## @brief   A run counter for Timer1 task.
        #  @details Describes the number of times that the task has been
        #           executed by the task loop running in main. But only when
        #           the counting_flag is set.
        #
        self.counter = 0

        ## @brief   A run flag for Timer1 task.
        #  @details Describes whether or not the timer has already been set by a previous
        #           '1' key press.
        #
        self.counting_flag = 0

    def run(self):
        '''
        @brief      Task Timer1 run function
        @details    This method executes one single iteration of Timer1.

                    Print statement only used for debugging!
                    This task checks if the '1' key is pressed and timer isn't already
                    active. If its already counting then it keeps counting until the run counter
                    is an integer multiple of 1200 and then clears the counting_flag and counter.
                    The move_candle1_flag also gets set for MoveToCandle task.
                    The count of 1200 represents 12 seconds:
                    @f[ 1200 \times 10000~uS = 12~s @f]
        '''
        global key, move_candle1_flag

        if (key == '1') and self.counting_flag == 0:
            self.counting_flag = 1
            print("counting flag set")

        elif self.counting_flag == 1:
            self.counter += 1
            if self.counter % 1200 == 0:  ## if reached 12 seconds
                self.counter = 0
                self.counting_flag = 0
                move_candle1_flag = 1
                print("reached 12 seconds")
                key = '2'


class Timer2():
    '''
    @brief      Timer2
    @details    This class defines Timer2 task. In this task, if the most recent
                key press was a '2' and the timer isn't already set, then starts
                the timer. The timer counts for 25 seconds. Then the counting_flag
                clears and the move_candle1_flag gets set for the MoveToCandle task.
    '''

    def __init__(self):
        '''
        @brief      Constructor for Timer2
        @details    This constructor initializes the self.counter and self.counting_flag
                    variables.
        '''

        print('Created Timer2 Object')
        ## @brief   A run counter for Timer2 task.
        #  @details Describes the number of times that the task has been
        #           executed by the task loop running in main. But only when
        #           the counting_flag is set.
        #
        self.counter = 0

        ## @brief   A run flag for Timer2 task.
        #  @details Describes whether or not the timer has already been set by a previous
        #           '2' key press.
        #
        self.counting_flag = 0

    def run(self):
        '''
        @brief      Task Timer2 run function
        @details    This method executes one single iteration of Timer2.

                    Print statement only used for debugging!
                    This task checks if the '2' key is pressed and timer isn't already
                    active. If its already counting then it keeps counting until the run counter
                    is an integer multiple of 2500 and then clears the counting_flag and counter.
                    The move_candle1_flag also gets set for MoveToCandle task.
                    The count of 2500 represents 25 seconds:
                    @f[ 2500 \times 10000~uS = 25~s @f]
        '''
        global key, move_candle2_flag

        if (key == '2') and self.counting_flag == 0:
            self.counting_flag = 1
            print("counting flag 2 set")

        elif self.counting_flag == 1:
            self.counter += 1
            if self.counter % 2500 == 0:  ## if reached 25 seconds
                self.counter = 0
                self.counting_flag = 0
                move_candle2_flag = 1
                print("timer 2 reached 10 seconds")
                key = None


# ------------------------------------------------------------------------------
class MoveToCandle:
    '''
    @brief      MoveToCandle
    @details    This class is set up to to determine when and where the platform 
                will go to when the timer for either cande 1 or candle 2 is done.
                this also set up the flag to triger the close lid class once the
                candle platform has reached its location.
    '''

    def __init__(self):
        '''
        @brief      Constructor for task MovetoCandle
        @details    all pins have been set in the main section of the code to 
                    be accessed by the other classes.
        '''

        print('Created MoveToCandle Object')


    def run(self):
        '''
        @brief      MoveToCandle run function
        @details    depending on what candle is chosen the timer for that flag sets
                    this section of the code. The first run through sets the kp and 
                    set point values to reach the candle position. On the next run 
                    it turns the motor on and keeps it going until the desired 
                    position has been reached. Once the desired position has been 
                    reached the close lid flag and the stay flag is set. the stay 
                    flag is set so that the if the second candle flag goes off as 
                    while the first candle falg is still runing through its process 
                    it will stay in position until the candle has been extinguished 
                    before moving on to extinguish the second candle
        '''
        global stay_flag, move_candle1_flag, move_candle2_flag, \
                moving1_flag, moving2_flag, closelid_flag, setpoint, \
                actuation

        if stay_flag == 1:
            pass

        elif move_candle1_flag == 1 and moving1_flag == 1:
            pos = encplat.get_position()

            if pos < setpoint + 25 and pos > setpoint - 25:
                print("arrived at position 1")
                moving1_flag = 0
                moeplat.disable()

            else:
                print("moving motor to pos 1")
#                 moeplat.enable()
                encplat.update()
                moeplat.set_duty(actuation.update(encplat.get_position()))


        elif move_candle1_flag == 1 and moving1_flag == 0:
            if hall_sensor.OnOff() == 0:  # platform at home
                print("platform at home. setting up kp")
                moving1_flag = 1
                kp = .75
                setpoint = 12000
                actuation = KPControl(setpoint, kp)
                moeplat.enable()
                moeplat.set_duty(actuation.update(encplat.update()))
                utime.sleep_ms(10)
            else:  # at position 1 ## PROBLEM: when not at home and need to move to position 1
                print("platform at position 1")
                closelid_flag = 1
                stay_flag = 1
                move_candle1_flag = 0

        elif move_candle2_flag == 1 and moving2_flag == 1:
            pos = encplat.get_position()
            if pos < setpoint + 25 and pos > setpoint - 25:
                moving2_flag = 0
                moeplat.disable()
            else:
                moeplat.enable()
                encplat.update()
                moeplat.set_duty(actuation.update(encplat.get_position()))


        elif move_candle2_flag == 1 and moving2_flag == 0:
            if hall_sensor.OnOff() == 0:
                moving2_flag = 1
                kp = .75
                setpoint = 2000
                actuation = KPControl(setpoint, kp)
                moeplat.enable()
                moeplat.set_duty(actuation.update(encplat.update()))
            else:  ## PROBLEM: when not at home and need to move to position 1
                closelid_flag = 1
                stay_flag = 1
                move_candle2_flag = 0
        else:
            pass


# ----------------------------------------------------------------------------------
class MoveLid:
    '''
    @brief      MoveLid
    @details    This class is set up to to determine where the candle lid position is.
                this class is only called when the platform has reached its position or 
                the timer to open the lid has gone off.
    '''

    def __init__(self):
        '''
        @brief      Constructor for task MoveLid
        @details    all pins have been set in the main section of the code to 
                    be accessed by the other classes.
        '''

        print('Created MoveLid Object')

    def run(self):
        '''
        @brief      Task MoveLid run function
        @details    This code is only run when the close lid flag was been set 
                    form the move to candle class or when the open lid has been 
                    from the hold lid timer. On the first run through when the
                    close lid flag has been set the motor is initialized with the
                    kp value of .75 and the set point value of -210. The second 
                    the motor is turned on and then turned off once the setpoint 
                    has been reached. The hold lid counter is triggered. once the 
                    counter is don the open lid flag is set and the motor is initialized
                    agin but this time the set pint value is set to 0 (its initial starting point).
                    when the lid has reached its original starting point the go home 
                    flag is set so the the platform will drive to its home position
                    to wait for the next candle flag to be set.
        '''
        global stay_flag, move_candle1_flag, move_candle2_flag, \
            moving1_flag, moving2_flag, closelid_flag, kp, movinglid_flag, \
            holdlid_flag, openlid_flag, actuation, setpoint, gohome_flag

        if closelid_flag == 1 and movinglid_flag == 1:
            poslid = enclid.get_position()
            print("enclid pos: ", poslid)
            if poslid < setpoint + 10 and poslid > setpoint - 10:
                print("lid closed")
                moelid.disable()
                closelid_flag = 0
                movinglid_flag = 0
                holdlid_flag = 1

            else:
                print("closing lid")
                enclid.update()
                moelid.set_duty(actuation.update(enclid.get_position()))


        elif closelid_flag == 1 and movinglid_flag == 0:
            print("initializing move lid kp")
            movinglid_flag = 1
            setpoint = -210
            kp = 0.75
            actuation = KPControl(setpoint, kp)
            moelid.enable()
            moelid.set_duty(actuation.update(enclid.update()))

        elif openlid_flag == 1 and movinglid_flag == 1:
            poslid = enclid.get_position()
            print("enclid pos: ", poslid)

            if poslid < +25 and poslid > -25:
                print("lid opened going home")
                moelid.disable()
                openlid_flag = 0
                movinglid_flag = 0
                gohome_flag = 1
                moeplat.enable()

            else:
                print("opening lid")
                enclid.update()
                moelid.set_duty(actuation.update(enclid.get_position()))

        elif openlid_flag == 1 and movinglid_flag == 0:
            print("initializing move lid kp")
            movinglid_flag = 1
            setpoint = -70
            actuation = KPControl(setpoint, kp)
            moelid.enable()
            moelid.set_duty(actuation.update(enclid.update()))

        else:
            pass

# -------------------------------------------------------------------------------------

class GoHome:
    '''
    @brief      GoHome
    @details    this is ths first code that is executed to make sure that the plateform
                is in the home position before moving the plateform to the candle 1 or 
                candle 2 position.
    '''

    def __init__(self):
        '''
        @brief      Constructor for GoHome
        @details    all pins have been set in the main section of the code to 
                    be accessed by the other classes.
            
        '''
        # initialization of code to test if the hall effect sensor
        # will stop the DC motor

        print('Created GoHome Object')

    def run(self):
        global gohome_flag, stay_flag, athome_flag
        '''
        @brief      GoHome run function
        @details    This code is the very first thing that is run to ensure that the 
                     platform is in the home position. The motor is initalized and 
                    run at full power untill the hall effect sensor is triggered by 
                    the magnet glued to the plateform. This section of the code is also 
                    triggered once the lid has been open. this ensure that the motor 
                    always is calibrated to the home position before moving on the the
                    next candle.
        '''
        if gohome_flag == 1:
            if hall_sensor.OnOff() == 1:
                print("going home")
                moeplat.set_duty(-100)
                hall_sensor.OnOff()
            else:  # at home
                print("at home")
                moeplat.disable()
                gohome_flag = 0
                stay_flag = 0
                encplat.zero()



#-------------------------------------------------------------------------------------
class HoldLidTimer:
    '''
    @brief      HoldLidTimer
    @details    This class defines the HoldLidTimer task. The task
                checks if the global holdlid_flag is set. If it is then it counts 7 seconds
                and then clears holdlid_flag and setsopenlid_flag for the MoveLid task.
    '''

    def __init__(self):
        '''
        @brief      Constructor for HoldLidTimer
        @details    Timing counter self.counter initialized.
        '''

        print('Created HoldLidTimer Object')

        ## @brief   A run counter for HoldLidTimer task.
        #  @details Describes the number of times that the task has been
        #           executed by the task loop running in main. But only while
        #           holdlid_flag is set.
        #
        self.counter = 0

    def run(self):
        '''
        @brief      HoldLidTimer run function
        @details    This method executes one single iteration of the task.

                    Print statement only used for debugging!
                    This task only does anything nontrivial if the holdlid_flag is on
                    and the counter reaches 700 (for 7 seconds). Then the holdlid_flag is cleared and
                    the openlid_flag is set. The value of 700 comes from
                    @f[ 700 \times 10000~uS = 7~s @f].
        '''
        global holdlid_flag, openlid_flag

        if holdlid_flag == 1:
            self.counter += 1
            if self.counter % 700 == 0:  # if reached 7 seconds
                self.counter = 0
                holdlid_flag = 0
                openlid_flag = 1
                print("lid held for 5 seconds")



#--------------------------------------------------------------------------------------
##
# @brief    List of task classes
# @details  This Python list contains all of the objects representing the
#           different tasks that will be called during the task loop.
#
task_list = [Timer1(), Timer2(), GoHome(), MoveToCandle(), MoveLid(), HoldLidTimer()]

## @brief   Task loop rate in uS
#  @details A Python integer that represents the nominal number of microseconds
#           between iterations of the task loop.
#
interval = 10000

## @brief   The timestamp of the next iteration of the task loop
#  @details A value from utime.ticks_us() specifying the timestamp for the next
#           iteration of the task loop.
#
#           This value is updated every time the task loop runs to schedule the
#           next iteration of the task loop.
#
next_time = utime.ticks_add(utime.ticks_us(), interval)

## @brief   The timestamp associated with the current iteration of the task
#           loop
#  @details A value from utime.ticks_us() updated every time the task loop
#           checks to see if it needs to run. As soon as this timestamp exceeds
#           next_time the task loop runs an iteration.
#
cur_time = utime.ticks_us()

while True:
    # Update the current timestamp
    cur_time = utime.ticks_us()

    # Check if its time to run the tasks yet
    if utime.ticks_diff(cur_time, next_time) > 0:

        # If it is time to run, update the timestamp for the next iteration and
        # then run each task in the task list.
        next_time = utime.ticks_add(next_time, interval)
        for task in task_list:
            task.run()
    if key == "A":
        print("leaving program")
        break


