'''
@file       task_movelid.py
@brief      Example task #6
@details    This file is an example dummy task showing how to implement a task
            using a Python class.
            
@page       page_task_6 MoveLid

@brief      MoveLid Documentation

@details

@section    sec_task_6_intro Introduction
            This task prints a timestamp every 1 second.

@section    sec_task_6_fsm Finite State Machine
            This task is implemented using a finite state machine with one
            state.
@subsection sec_task_6_trans State Transition Diagram
@image      html task_6_fsm.svg width=600px

@section    sec_task_6_imp Implementation
            This task is implemented by the MoveLid class.
'''

import utime

class MoveLid:
    '''
    @brief      MoveLid
    @details    This class defines a dummy task to use as an example. The task
                simply prints a timestamp to the terminal every 1 second.
    '''

    def __init__(self):
        '''
        @brief      Constructor for task 6
        @details    This constructor is where any initialization code would run
                    to configure the task before it starts operating. This isn't
                    really the same as an "init" state that you would have in a
                    finite state machine. Its more about setting up Python
                    objects that you will need to use in your task.
        '''

        print('Created MoveLid Object')

    ## @brief   A run counter for task 6
    #  @details Describes the number of times that the task has been
    #           executed by the task loop running in main. This counter can
    #           be used to perform things at a regualr interval as shown in
    #           the run function below.
    #

    def run(self):
        '''
        @brief      Task 1 run function
        @details    This method executes one single iteration of task 6. It is
                    up to you how you structure the code within this method, but
                    a finite state machine would be a good implementation if the
                    task is sufficiently complicated.

                    This task only does anything nontrivial if the run counter
                    is an integer multiple of 100. This forces the print
                    statement below to run about once per second because
                    @f[ 100 \times 10000~uS = 1~s @f]
        '''
        global stay_flag, move_candle1_flag, move_candle2_flag, \
            moving1_flag, moving2_flag, closelid_flag, kp, movinglid_flag, \
            holdlid_flag, openlid_flag, actuation, setpoint, gohome_flag

        if closelid_flag == 1 and movinglid_flag == 1:
            poslid = enclid.get_position()

            if poslid < setpoint + 25 and poslid > setpoint - 25:
                moelid.disable()
                closelid_flag = 0
                movinglid_flag = 0
                holdlid_flag = 1

            else:
                moelid.enable()
                enclid.update()
                moelid.set_duty(actuation.update(enclid.get_position()))


        elif closelid_flag == 1 and movinglid_flag == 0:
            movinglid_flag = 1
            setpoint = 80
            actuation = KPControl(setpoint, kp)
            moelid.enable()
            moelid.set_duty(actuation.update(enclid.update()))

        elif openlid_flag == 1 and movinglid_flag == 1:
            poslid = enclid.get_position()

            if poslid < +25 and poslid > -25:
                moelid.disable()
                openlid_flag = 0
                movinglid_flag = 0
                gohome_flag = 1
            else:
                moelid.enable()
                enclid.update()
                moelid.set_duty(actuation.update(enclid.get_position()))

        elif openlid_flag == 1 and movinglid_flag == 0:
            movinglid_flag = 1
            setpoint = 80
            actuation = KPControl(setpoint, kp)
            moelid.enable()
            moelid.set_duty(actuation.update(enclid.update()))

        else:
            pass
