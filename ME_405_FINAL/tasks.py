##
# @page page_tasks  Task List
#                   This project is implemented using a task-based paradigm.
#                   The following tasks are present:
#                   - @subpage page_task_1 "Task 1 - Example Task #1"
#                   - @subpage page_task_2 "Task 2 - Example Task #2"
#                   - @subpage page_task_3 "Task 3 - Example Task #3"
#
#                   All tasks run cooperatively at an interval of 10 mS.