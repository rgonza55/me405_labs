'''
@file       check_btn.py
@brief      Example task #1
@details    This file is an example dummy task showing how to implement a task
            using a Python class.
            
@page       page_task_1 Task 1

@brief      Task 1 Documentation

@details

@section    sec_task_1_intro Introduction
            This task prints a timestamp every 1 second.

@section    sec_task_1_fsm Finite State Machine
            This task is implemented using a finite state machine with one
            state.
@subsection sec_task_1_trans State Transition Diagram
@image      html task_1_fsm.svg width=600px

@section    sec_task_1_imp Implementation
            This task is implemented by the Task_1 class.
'''

import utime
from keypad import Keypad

class CheckBtn:
    '''
    @brief      Task 1: CheckBtn
    @details    This class defines the task of saving the char key value from
                the keypad button press.
    '''

    def __init__(self):
        '''
        @brief      Constructor for task 1
        @details    This constructor is where any initialization code would run
                    to configure the task before it starts operating. This isn't
                    really the same as an "init" state that you would have in a
                    finite state machine. Its more about setting up Python
                    objects that you will need to use in your task.
        '''

        print('Created CheckBtn Object')


    def run(self):
        '''
        @brief      Task: CheckBtn 1 run function
        @details    This method always saves a key value from the keypad.
        '''
        global key
        key = self.keypad.get_key()

        ## Only used for testing/debuggin:
        # if key:
        #     print("key is: ", key)

