'''
@file       task_movetocandle.py
@brief      Move to cadle task #4
@details    This file is an example dummy task showing how to implement a task
            using a Python class.
            
@page       page_task_1 MoveToCandle

@brief      MoveToCandle Documentation

@details

@section    sec_task_4_intro Introduction
            This task prints a timestamp every 1 second.

@section    sec_task_4_fsm Finite State Machine
            This task is implemented using a finite state machine with one
            state.
@subsection sec_task_4_trans State Transition Diagram
@image      html task_4_fsm.svg width=600px

@section    sec_task_4_imp Implementation
            This task is implemented by the MoveToCandle class.
'''

import utime

class MoveToCandle:
    '''
    @brief      MoveTo Candle
    @details    This class defines a dummy task to use as an example. The task
                simply prints a timestamp to the terminal every 1 second.
    '''

    def __init__(self):
        '''
        @brief      Constructor for task 4
        @details    This constructor is where any initialization code would run
                    to configure the task before it starts operating. This isn't
                    really the same as an "init" state that you would have in a
                    finite state machine. Its more about setting up Python
                    objects that you will need to use in your task.
        '''

        print('Created MoveToCandle Object')

    ## @brief   A run counter for task 1
    #  @details Describes the number of times that the task has been
    #           executed by the task loop running in main. This counter can
    #           be used to perform things at a regualr interval as shown in
    #           the run function below.
    #
    # self.runs = 0

    def run(self):
        '''
        @brief      MoveToCandle run function
        @details    This method executes one single iteration of task 4. It is
                    up to you how you structure the code within this method, but
                    a finite state machine would be a good implementation if the
                    task is sufficiently complicated.

                    This task only does anything nontrivial if the run counter
                    is an integer multiple of 100. This forces the print
                    statement below to run about once per second because
                    @f[ 100 \times 10000~uS = 1~s @f]
        '''
        global stay_flag, move_candle1_flag, move_candle2_flag, \
                moving1_flag, moving2_flag, closelid_flag, setpoint, \
                actuation

        if stay_flag == 1:
            pass

        elif move_candle1_flag == 1 and moving1_flag == 1:
            pos = encplat.get_position()

            if pos < setpoint + 25 and pos > setpoint - 25:
                move_candle2_flag = 0
                moeplat.disable()

            else:
                moeplat.enable()
                encplat.update()
                moeplat.set_duty(actuation.update(encplat.get_position()))


        elif move_candle1_flag == 1 and moving1_flag == 0:
            if hall_sensor.OnOff() == 0:  # need to make this global
                moving2_flag = 1
                kp = .75
                setpoint = 12000
                actuation = KPControl(setpoint, kp)
                moeplat.enable()
                moeplat.set_duty(actuation.update(encplat.update()))
            else:
                closelid_flag = 1
                stay_flag = 0
                move_candle1_flag = 0

        elif move_candle2_flag == 1 and moving2_flag == 1:
            pos = encplat.get_position()
            if pos < setpoint + 25 and pos > setpoint - 25:
                move_candle2_flag = 0
                moeplat.disable()
            else:
                moeplat.enable()
                encplat.update()
                moeplat.set_duty(actuation.update(encplat.get_position()))


        elif move_candle2_flag == 1 and moving2_flag == 0:
            if hall_sensor.OnOff() == 0:
                moving2_flag = 1
                kp = .75
                setpoint = 2000
                actuation = KPControl(setpoint, kp)
                moeplat.enable()
                moeplat.set_duty(actuation.update(encplat.update()))
            else:
                closelid_flag = 1
                stay_flag = 1
                move_candle2_flag = 0
        else:
            pass
