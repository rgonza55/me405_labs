## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  Spring 2020 ME405 Mechatronics Final Project Files
#
#  @section sec_pages Contents:
#  (click to open)\n
#  @ref page1
#  \n
#  <hr>
#  @authors Jenny Chiao and Raul Gonzalez
#
#  @copyright License Info
#
#  @date May 25, 2020
#
#
#
#  @page page1 Project Proposal: Candle Extinguisher
#  @subsection Problem Statement
#  Scented candles are a nice way to to set a relaxing environment for any room with its 
#  warm light and pleasant aromas. But they can be dangerous if a person falls asleep 
#  without putting the flame out. According to the National Fire Protection Association 
#  (NFPA),a 2013-2017 study showed about 1/3 of home candle fires start in the home 
#  bedroom. Falling asleep accounted for 11% of those incidents.
#  \n\n
#  Our project is a device that puts out the candle with just the push of remote control 
#  button. Multiple candles can be placed on a platform. There will be two motors 
#  utilized. One servo motor will be used to hinge a 3D printed arm holding a metal lid. 
#  The lid is lowered over the candle and held there for a set period of time to guarantee #  the candle flames are extinguished from lack of oxygen. A stepper motor will be used on
#  a guide rail and belt system to horizontally move the lid holding arm to the correct 
#  candle location. An infrared remote controller for the user allows for an easy way to 
#  safely extinguish the candles from across the room. If the use is relaxed and 
#  comfortable, its simple to just press a button without too much thought. 
#
#  \n
#  <hr>
#
#  @subsection Project Requirements
#  The STM32 Nucleo board running MicroPython will be used to control the motors and IR
#  sensor.
#  \n\n
#  The stepper motor and servo motors will serve as our two actuators for horizontal and 
#  rotational movement in the system. The stepper motor needs to move the arm and lid to 
#  the correct candle location. We may use an encoder with this for feedback position 
#  control. We may also decide to utilize the brushless dc motors from our lab kits if 
#  they are viable solutions that work with our required load. The servo motor will need
#  to lower the arm and lid until its placed over the candle. For this project we are 
#  assuming all candles will be the same size and height. 
#  \n\n
#
#  We are also utilizing an infrared receiver sensor to be used with a remote controller. 
#  Only two buttons will be used. The “1” button is used to turn off candle in position 1 
#  and “2” for candle in position 2.
#
#  \n
#  <hr>
#
#  @subsection Materials
#  For this project we will be using the Nucleo-STM32L476 board, the motor driver
#  shield, and at least one of the bread boards provided to us. The rest of the electrical #  components needed will be purchased through Amazon or Digikey. The building material   #  for this project will be 3D printed and off the shelf components including a            #  linear guide rail system. The full list of all the material we intend to use is shown    #  in Table 1 in the bill of material subsection. 
#   
# 
#
#  
#  @subsubsection Bill of Materials
#
#
#
#
#  \n
#  <hr>
#
#  @subsection Assembly Plan
#  Sub assembly 1(base components) \n
#  - Linear guide rail system \n
#  - 3d printed base for arm \n
#  - Wood piece \n
#  - Stepper motor \n
#  - 3d printed “tower” for ir sensor \n 
#  - 4? Bolts \n
#  - Mat for candle placement \n
#  @image html sub_assembly_1.JPG "Figure 1: Manufacturing steps for subassembly 1"
#  \n
#  \n\n
#  Sub assembly 2 (arm mechanism)\n
#  - 3D printed arm \n
#  - Metal Lid \n
#  - Pin \n
#  - Servo motor \n 
#  @image html sub_assembly_2.JPG “Figure 2: Manufacturing steps for subassembly 2”
#  \n
#  \n\n
#  Sub assembly 3 (electrical housing) \n
#  - 3D printed housing  \n
#  - Nucleo board \n
#  - DC motor shield \n 
# @image html sub_assembly_3.JPG “Figure 3: Manufacturing steps for subassembly 3”
#  \n
#  <hr>
#
#  @subsection Safety Assessment
#  Working with lit candles is a hazard. We will be strategic to use materials that don’t
#  easily deform or melt and place them so they are out of the candle flame’s way. Our 
#  system will also be tested in a cleared space with all flammable materials out of the 
#  way. We’ll have a fire extinguisher and bucket of water ready in case anything happens.   
#  \n\n
#  We have one rotating component with the lid and arm mechanism that may create a
#  potential pinch point. Since the parts are not very heavy this shouldn’t cause any 
#  serious damage. The rail system may also cause damage if a hand or finger is on the 
#  rail while the mechanism is trying to move across. Again, parts are not very heavy but 
#  we will make sure to clear the rail and space around the system before testing. 
#
#  \n
#  <hr>
#
#  @subsection Project Timeline
#  We have a little under 3 weeks before the deadline of June 12. Here is our plan:\n
#  Week of May 24: \n
#  - Research and order parts by Tuesday at the latest \n
#  - Sketch CAD model of system \n
#  - Begin drafts for 3D printed parts \n
#  - Determine electrical pinouts for Nucleo board and peripherals\n
#  - Begin programming code for motors based on datasheets
#  \n\n
#  Week of May 31: \n
#  - Start 3D printing parts\n
#  - Testing/debugging motor driver codes on Nucleo board\n  
#  - Test servo motor with arm + lid attached on unlit candle (sub assembly 2) \n
#  - Test sub assembly 2 with a live candle and ensure nothing is melting \n 
#  - Assemble rail and belt system and test with stepper motor (sub assembly 1)\n
#  - Write code for IR remote controller and sensor \n
#  - Start writing main file for whole system \n
# \n\n
# Week of June 8: \n
# - Finish testing IR sensor code and add sensor to system \n
# - 3D print electrical housing \n
# - Complete writing main system file by Tuesday \n
# - Test/debug operation of entire system together with a live candle 
#  \n
#  <hr>

