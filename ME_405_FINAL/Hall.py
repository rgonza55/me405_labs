## @file Hall.py
#  Python source file to use a Hall Effect switch.
#
#  @package hall
#  Contains class with a method to read from sensor.
#

import pyb


class HallEffectSensor:
	'''reads the input value of the hall effect sensor.
    @parm read_pin a pyb Pin object to use as the input pin of the sensor
    '''

	def __init__(self, read_pin):
		'''enables the hall effect sensor to send signals to the nucleo board
        '''
		self.read_pin = read_pin
		print('making hall sensor pin')

	def OnOff(self):
		'''returns the value of the hall effect sensor pin
        if value of 1 is returned then the hall effect sensor has not been triggered
        if value of 0 is returned then the hall effect senor has been triggered
        '''
		return (self.read_pin.value())


if __name__ == '__main__':
	read_pin = pyb.Pin(pyb.Pin.cpu.A4, pyb.Pin.IN);
	hall = HallEffectSensor(read_pin)

