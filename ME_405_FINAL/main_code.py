##@file.main_code.py

from Scratch import pyb

from motordriver import MotorDriver
from encoder import Encoder

# Initializing motor pins
pin_en = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)
pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)
pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)

# Create the timer object used for PWM generation
tim = pyb.Timer(3, freq=20000)

## A motor driver object
motor1 = MotorDriver(pin_en, pin_IN1, pin_IN2, tim)

# Initializing encoder pins
pin_B6 = pyb.Pin(pyb.Pin.cpu.B6)
pin_B7 = pyb.Pin(pyb.Pin.cpu.B7)
setperiod = 65535
timer4 = pyb.Timer(4, prescaler=0, period=setperiod)

## Encoder driver object:
enc1 = Encoder(pin_B6, pin_B7, timer4, setperiod)




'''@main_code.py
'''

def HomePosition():
    ''' 
    stepper motor will need to know where home position is. this weill be off 
    to the side. the smaller dc motor witht the encoder will need to be 
    straight up upon initialization.
    '''
def HoldPosition():
    '''
    set timmer up to hold the position of the smaller motor with the encoder
    for (10 seconds?) after this time signal to start rasing the small DC motor
    '''
def MoveDCMotor():
    ''' 
    move small DC motor clockwise.
    move small DC motor CCW 
    is it possible to know if the small DC motor is drawing its max amperage 
    and voltage?
    '''
def
