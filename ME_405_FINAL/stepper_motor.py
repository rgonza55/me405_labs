##@stepper_motor.py

'''@stepper_motor.py
'''
from Scratch import pyb


class StepperMotorDriver:
    def __init__(self, step_pin, dir_pin, timer):
        '''
        this class is used to drive a stepper motor with 
        4 wire stepper motor 
        '''
        self.step_pin=step_pin
        self.dir_pin=dir_pin
        self.timer=timer
        
        print('creating stepper motor driver')  
    
    def CCW(self):
        self.dir_pin.high()
        tmch1.pulse_width_percent(50)
        
        
    def CW(self):
        self.dir_pin.low()
        tmch1.pulse_width_percent(50)
        
    def disable(self):
        tmch1= self.timer.channel(1, pyb.Timer.PWM, pin=self.step_pin)
        tmch1.pulse_width_percent(0)          
          
        '''
        tmch2= self.timer.channel(2, pyb.Timer.PWM, pin=self.dir_pin)
        tmch2.pulse_width_percent(0)
        probably do not need this section of code
        '''
        
        
if __name__ == '__main__':

    '''sets the pins for the step input and the direction input of the stepper 
    motor'''
    # Pin A0 will be reciving the pulse signals for wach step this pin is 
    # designed to be used with timer 5. timer 3 can also be used for this,
    # however the pins needed would be B4 and B5
    step_pin= pyb.Pin(pyb.Pin.cpu.A0, pyb.Pin.OUT_PP);
    
    # Pin A1 will be either set to low of high depending the direction we 
    # want it to go.
    dir_pin= pyb.Pin(pyb.Pin.cpu.A1, pyb.Pin.OUT_PP);
    
    # Create the timer object used for PWM generation
    # If the B-PINS are used the timer channel must be 3
    # If the A-Pins are used the timer channel must be 5
    tim = pyb.Timer(5, freq =2000);
    
    # Create a motor object passing in the pins and timer
    step = StepperMotorDriver(step_pin, dir_pin)
    
    # Enable the motor driver
    step.disable()
    
    
    

