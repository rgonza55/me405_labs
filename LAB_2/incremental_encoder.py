##@incremental_encoder.py

'''@incremental_encoder.py
'''
import pyb

class IncrementalEncoder:
    ''' This class implements an encoder reader for the DC motor used with the 
    ME405 board. '''
    
    def __init__ (self, Ch1_pin, Ch2_pin, timer):
        ''' Creates a incremental encoder reader by initializing GPIO
        pins and turning the motor off for safety.
        @param Ch1_pin A pyb.Pin object to use as channel 1 of the timmer.
        @param Ch2_pin A pyb.Pin object to use as channel 2 of the timmer.
        @param timA A pyb.Timer object to use as a counter for Ch1_pin and 
        Ch2_pin'''
        
        self.Ch1_pin=Ch1_pin
        self.Ch2_pin= Ch2_pin
        self.timer=timer
        
        self.position=self.timer.counter()
        self.position_1=self.timer.counter()
        self.position_2= self .timer.counter()
        
        print ('Creating a incremental encoder')
        
        
    def update (self):
        ''' update function calls the timmer counter to get the number
        of ticks that have have happened. This numberis then returned to the 
        user.Once the counter reaches the set period it resets and starts 
        counting form 0 again.'''
        # after settig each of the position variables to read the encoder
        self.position_2=self.position_1
        self.position_1=self.timer.counter()

        self.position=self.position+self.get_delta()
        
        return(self.position)
    
       

    def get_position (self):
        '''updates the to the most recent position of the motor'''
        # returns the value of the current position of the motor
        return (self.position)
        
            
    def set_position (self, user_set_position): 
        ''' This method lest the user set the desired position of the 
        motor. Positive values cause effort in one direction, 
        negative values in the opposite direction.
        @param user_set_position A signed integer holding the desired
        location of the motor.'''          
        self.user_set_position=user_set_position
        # returns the value of user set position
        return(user_set_position)
                
    def get_delta (self):
        '''updates the difference between the current position and 
        user set position. overflow and underflow are both acounted 
        for and delt with accodingly.'''
        # delta used as variable to hold the difference between the 
        #user set poistion and the current position of the motor
        
        delta = self.position_1-self.position_2
        # delta value returned if there is any underflow
        if delta > ((timer.period())/2):
            delta=delta-timer.period()
            print('underflow')
        # delta value returned if there is any overflow
        elif delta < (-(timer.period())/2):
            delta=delta+timer.period()+delta
            print('overflow')
        # delta value returned if there is no overflow or underflow
        
        return(delta)
    
    def ZeroAll (self):
        self.position=0
        self.position_1=0
        self.position_2=0
        self.timer.counter(0)
        print ('All parameters have been zeroed out')
       
if __name__ == '__main__':
    
    # Ch1_pin is channel 1 used for timmer 4 which corresponds to pin B6
    # Ch2_pin is channel 2 used for timmer 4 which corresponds to pin B7
    # Ch1_pin can be changed to pin C6 and Ch2_pin can be changed to pin C7
    # these two pins can be used in conjection with timmer 8
    Ch1_pin = pyb.Pin (pyb.Pin.cpu.B6) ;
    Ch2_pin = pyb.Pin (pyb.Pin.cpu.B7);
    timer = pyb.Timer(4, prescaler=0, period=1400);
    
    # setting the Ch1_pin and Ch2_pin to be trigger when channel 1 or 
    # channel 2 is changed from high to low or low to high
    timAch1=timer.channel(1, pyb.Timer.ENC_A, pin=Ch1_pin);
    timAch2=timer.channel(2, pyb.Timer.ENC_B, pin=Ch2_pin);
    
    # Create a encoder object passing in the pins and timer
    enc=IncrementalEncoder(Ch1_pin, Ch2_pin, timer)
    
    # sets the position of the desired position to 300 "ticks"
    enc.user_set_position= (300)





